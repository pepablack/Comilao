package br.pedro.paulo.monteiro.comilao;

import java.util.Arrays;

public class DefinicaoAlimentos {

	public static final int[] alimentos = {R.drawable.apple_r, R.drawable.apple,
			R.drawable.ball3, R.drawable.ball8, R.drawable.banana,
			R.drawable.bat, R.drawable.bee, R.drawable.beer, R.drawable.bread,
			R.drawable.brush, R.drawable.cake, R.drawable.candy1,
			R.drawable.corn, R.drawable.dice, R.drawable.glass,
			R.drawable.hamburger, R.drawable.hat, R.drawable.hat2,
			R.drawable.hotdog, R.drawable.jelly,
			R.drawable.ladybug, R.drawable.lemon, R.drawable.lemonade,
			R.drawable.magnet, R.drawable.meat, R.drawable.pineapplet,
			R.drawable.pizza, R.drawable.poison, R.drawable.rice,
			R.drawable.socks, R.drawable.soda, R.drawable.soup,
			R.drawable.tennis, R.drawable.wheel };
	
	private static int[] alimentosValidos = {R.drawable.apple,
		R.drawable.banana, R.drawable.bread, R.drawable.cake, R.drawable.candy1,
		R.drawable.corn, R.drawable.hamburger, R.drawable.hotdog, R.drawable.jelly,
		R.drawable.lemon, R.drawable.lemonade, R.drawable.meat, R.drawable.pineapplet,
		R.drawable.pizza, R.drawable.rice, R.drawable.soda, R.drawable.soup};
	
	public static boolean alimentoContaPonto(int idAlimento){
		Arrays.sort(alimentosValidos);
		if (Arrays.binarySearch(alimentosValidos, idAlimento) >= 0)
			return true;
		return false;
	}
}
