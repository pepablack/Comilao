package br.pedro.paulo.monteiro.comilao;

import java.util.Random;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class Alimento {
	
	private Drawable myDrawable;
	private int mIdDrawable;
	private int largura;
	private int altura;
	private int posicaoX;
	private int posicaoY;
		
	public Alimento(Context context, int idDrawable) {
		myDrawable =  context.getResources().getDrawable(idDrawable);
		mIdDrawable = idDrawable;
		largura = myDrawable.getIntrinsicWidth();
		altura = myDrawable.getIntrinsicHeight();
	}
	
	public int getIDAlimento(){
		return mIdDrawable;
	}
	public void moverAlimentoBaixo(){
		posicaoY = posicaoY + 1;
		setPosicao();
	}
	public void setPosicaoInicialRandom(int larguraViewPai){
		posicaoX = new Random().nextInt(larguraViewPai);
		posicaoY = 0;
		setPosicao();
	}
	private void setPosicao(){
		myDrawable.setBounds(posicaoX, posicaoY, posicaoX + largura, posicaoY + altura);
	}
	
	public void draw(Canvas canvas){
		myDrawable.draw(canvas);
	}
	
	public Rect getBounds(){
		return myDrawable.copyBounds();
	}

}
