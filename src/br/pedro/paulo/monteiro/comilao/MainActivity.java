package br.pedro.paulo.monteiro.comilao;

import java.util.Random;
import java.util.Timer;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;

public class MainActivity extends ActionBarActivity implements SensorEventListener {
	
	
	private Sensor mAccelerometer;
	private SensorManager mSensorManager;
	private MediaPlayer mMediaPlayer, mWin, mLose;
	private Comilao mComilao;
	private Timer mTimerCriaAlimento;
	private Timer mTimerAnimaAlimento;
	private static int TEMPO_CRIACAO_ALIMENTO = 1300;
	private static int TEMPO_ANIMACAO_ALIMENTO = 5;
		
	private Handler mHandlerCriaAlimento = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			int[] alimentos = DefinicaoAlimentos.alimentos;
			int randomAlimentoId = new Random().nextInt(alimentos.length);
			Alimento a = new Alimento(getApplicationContext(), alimentos[randomAlimentoId]);
			mComilao.adicionaAlimento(a);
			mComilao.invalidate();
		}
    };
    
    private Handler mHandlerAnimaAlimento = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			mComilao.desceAlimentos();
			mComilao.invalidate();
		}
    };
		
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComilao = new Comilao(this, this);

        setContentView(mComilao);
		ligaAcelerometro();
		ligaSom();
		
		mTimerCriaAlimento = new Timer();
		mTimerCriaAlimento.schedule(new TimerAnimacao(mHandlerCriaAlimento), 0, TEMPO_CRIACAO_ALIMENTO);
		
		mTimerAnimaAlimento = new Timer();
		mTimerAnimaAlimento.schedule(new TimerAnimacao(mHandlerAnimaAlimento), 0, TEMPO_ANIMACAO_ALIMENTO);
		
    }

	private void ligaSom() {
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mMediaPlayer = MediaPlayer.create(this, R.raw.music);
		mMediaPlayer.setLooping(true);
		
		mWin = MediaPlayer.create(this, R.raw.win);
		mLose =  MediaPlayer.create(this, R.raw.lose);
		
	}

	private void ligaAcelerometro() {
		mSensorManager =  (SensorManager) getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		return;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		int diferencaX, diferencaY;
		diferencaX = (int) event.values[0];
		diferencaY = (int) event.values[1];
		mComilao.atualizarPosicaoComilao(diferencaX, diferencaY);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		mMediaPlayer.start();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mMediaPlayer.start();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mMediaPlayer.pause();
	}
	
	public void finalizarJogo(boolean venceu){
		mTimerCriaAlimento.cancel();
		mTimerAnimaAlimento.cancel();
		mSensorManager.unregisterListener(this, mAccelerometer);
		mMediaPlayer.stop();
		
		if (venceu) {
			mWin.start();
			return;
		}
		mLose.start();
	}
	
}
