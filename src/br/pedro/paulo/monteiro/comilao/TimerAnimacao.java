package br.pedro.paulo.monteiro.comilao;

import java.util.TimerTask;
import android.os.Handler;

public class TimerAnimacao extends TimerTask {
	
	private Handler mHandler;
	
	public TimerAnimacao(Handler pHandler){
		mHandler = pHandler;
	}
	@Override
	public void run() {
		mHandler.sendEmptyMessage(1);
	}

}
