package br.pedro.paulo.monteiro.comilao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

public class Comilao extends View {

	private static int VIDA_MAXIMA_COMILAO = 3;
	private static int PONTO_MAXIMO_COMILAO = 15;
	private static int COEFICIENTE_SENSIBILIDADE_SENSOR = 5;
	
	private Drawable imgComilao;
	private ArrayList<Drawable> mVida;
	private MainActivity mActivity;
	private int mVidaComilao;
	private int mPontos;
	private int posicaoX, posicaoY;
	private int laguraComilao;
	private int alturaComilao;
	private int larguraView;
	private int alturaView;
	private MediaPlayer mEatSound;
	private MediaPlayer mNoSound;
	private CopyOnWriteArrayList <Alimento> meusAlimentos;
	
	public Comilao(Context context){
		super(context);
	}
	
	public Comilao(Context context, MainActivity mainActivity) {
		super(context);
		mActivity= mainActivity;
		setBackgroundResource(R.drawable.background);
		imgComilao = context.getResources().getDrawable(R.drawable.comilao);
				
		laguraComilao = imgComilao.getIntrinsicWidth();
		alturaComilao = imgComilao.getIntrinsicHeight();
		meusAlimentos = new CopyOnWriteArrayList <Alimento>();
		mVida = new ArrayList<Drawable>();
		mVidaComilao = VIDA_MAXIMA_COMILAO;
		iniciaVidasComilao();
		mPontos = 0;
		mEatSound = MediaPlayer.create(mainActivity, R.raw.eat);
		mNoSound = MediaPlayer.create(mainActivity, R.raw.no);
	}
	
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		posicaoX = w / 2 - (laguraComilao / 2);
		posicaoY = h - alturaComilao;
		larguraView = w;
		alturaView = h;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		desenharComilao(canvas);
		desenharAlimentos(canvas);
		desenharVidas(canvas);
	}

	public int getVidaComilao(){
		return mVidaComilao;
	}
	private void desenharAlimentos(Canvas canvas){
		for (Iterator<Alimento> iterator = meusAlimentos.iterator(); iterator.hasNext();) {
			Alimento mAlimento = (Alimento) iterator.next();
			alimentoColidiuComilao(mAlimento);
			mAlimento.draw(canvas);
		}
	}
	
	public void atualizarPosicaoComilao(int diferencaX, int diferencaY) {
		int novaposicaoX = (int) (posicaoX - diferencaX * COEFICIENTE_SENSIBILIDADE_SENSOR);
		if (novaposicaoX >= 0 && novaposicaoX <= (larguraView - laguraComilao)) {
			posicaoX = novaposicaoX;
			invalidate();
		}
	}
	
	public void adicionaAlimento(Alimento pAlimento) {
		pAlimento.setPosicaoInicialRandom(Math.max(this.getWidth()-100,1));
		meusAlimentos.add(pAlimento);
	}

	public void desceAlimentos() {
		for (Iterator<Alimento> iterator = meusAlimentos.iterator(); iterator.hasNext();) {
			Alimento mAlimento = (Alimento) iterator.next();
			mAlimento.moverAlimentoBaixo();
		}
	}
	
	private void iniciaVidasComilao(){
		Drawable vida = mActivity.getResources().getDrawable(R.drawable.heart);
		
		for (int i = 1; i <= VIDA_MAXIMA_COMILAO; i++) {
			Drawable newVida = vida.getConstantState().newDrawable();
			newVida.setBounds(larguraView + ((i-1)*vida.getIntrinsicWidth()),alturaView+ 10, larguraView + vida.getIntrinsicWidth() + ((i-1)*vida.getIntrinsicWidth()), alturaView + vida.getIntrinsicHeight());
			mVida.add(newVida);	
		}
	}
	
	private void desenharVidas(Canvas canvas){
		
		for (Iterator<Drawable> iterator = mVida.iterator(); iterator.hasNext();) {
			Drawable vida = (Drawable) iterator.next();
			vida.draw(canvas);
		}
	}
	
	private void desenharComilao(Canvas canvas) {
		imgComilao.setBounds(posicaoX, posicaoY, posicaoX + laguraComilao,	posicaoY + alturaComilao);
		imgComilao.draw(canvas);
	}
	
	private boolean alimentoColidiuComilao(Alimento pAlimento){
		Rect areaComilao = imgComilao.getBounds();
		Rect areaAlimento = pAlimento.getBounds();
		if (areaComilao.intersect(areaAlimento)){
			meusAlimentos.remove(pAlimento);
			atualizaPontuacaoAlimentoComido(pAlimento);
			return true;
		}
		return false;
	}

	private void atualizaPontuacaoAlimentoComido(Alimento pAlimento) {
		if (!DefinicaoAlimentos.alimentoContaPonto(pAlimento.getIDAlimento())) {
			mVida.remove(mVidaComilao - 1);
			mVidaComilao--;
			mNoSound.start();
			if (mVidaComilao == 0) {
				setBackgroundResource(R.drawable.game_over);
				mActivity.finalizarJogo(false);
				
			}

			Log.d("perdeu vida", mVidaComilao + "");
			return;
		}
		mPontos++;
		mEatSound.start();
		if (mPontos == PONTO_MAXIMO_COMILAO) {
			setBackgroundResource(R.drawable.you_win);
			mActivity.finalizarJogo(true);
		}
	}
}
